import axios from 'axios'

export default {
  logIn(credential){
    // return axios.post(process.env.VUE_APP_API_URL + '/login', credential)
    return axios({
      method: 'POST',
      baseURL: 'http://government.tuongdev.software/api/',
      url: 'login',
      data: credential
    })
  }
}