import axios from 'axios'
import store from '../store/index.js'

export default {
  getDonations(){
    // return axios.get(process.env.VUE_APP_API_URL + '/donation/getall')
    return axios.get('http://government.tuongdev.software/api/donation/getall')
  },
  createDonation(donation){
    let config = {
        headers: {
          Authorization: `Bearer ${store.getters.getToken}`
        }
      }
      // return axios.post(process.env.VUE_APP_API_URL + '/donation', donation, config)
      return axios.post('http://government.tuongdev.software/api/donation', donation, config)
  },
  closeDonation(id, body){
    let config = {
        headers: {
          Authorization: `Bearer ${store.getters.getToken}`
        }
      }
      // return axios.post(process.env.VUE_APP_API_URL + '/donation/disable/' + id, body, config)
      return axios.post('http://government.tuongdev.software/api/donation/disable/' + id, body, config)
  }
}