import { createRouter, createWebHistory } from "vue-router"
import Home from '@/views/Home.vue'
import Login from '@/views/Login.vue'
import NotFound from '@/views/NotFound.vue'
import Activity from '@/views/Activity.vue'
import Report from '@/views/Report.vue'

import store from '../store/index.js'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      title: "Trang chủ",
      requireAuth: true
    },
    children: [
      {
        path: '',
        name: 'Activity',
        component: Activity,
        meta: {
          title: "Hoạt động"
        },
      },
      {
        path: '/report',
        name: 'Report',
        component: Report,
        meta: {
          title: "Báo cáo"
        },
      },
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: {
      title: "Đăng nhập"
    },
  },
  {
    path: '/:pathMatch(.*)',
    component: NotFound,
    meta: {
      title: "Trang không tồn tại"
    },
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach(async (to, from, next) => {
  store.dispatch('checkAuth')
  if(to.matched.some(record => record.meta.requireAuth)) {
    if(!store.getters.isAuthenticated){
      next({ name: 'Login', query: {link: from.fullPath}})
    }
    else next()
  }
  else next()
})

const DEFAULT_TITLE = 'Gov Donate'
router.afterEach((to) => {
  document.title = (to.meta.title + ' - ' + DEFAULT_TITLE) || DEFAULT_TITLE;
});

export default router;
